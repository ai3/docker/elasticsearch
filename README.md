docker-elasticsearch
===

Simple Docker image for Elasticsearch, built using Debian packages.
Includes a Prometheus exporter.

The image expects /etc/elasticsearch to be provided externally, and
the following environment variables should be defined:

* `PORT` - the port for ES to listen on
* `EXPORTER_PORT` - the port for the exporter

You should probably also mount /var/log/elasticsearch and
/var/lib/elasticsearch to external storage.
