#!/bin/sh
#
# Install script for elasticsearch inside a Docker container.
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="ca-certificates curl rsync apt-transport-https"

# Packages to install.
PACKAGES="
	locales
	elasticsearch-oss
	elasticsearch-curator
"

# Release tarball for the elasticsearch prometheus exporter.
EXPORTER_RELEASE_URL="https://github.com/justwatchcom/elasticsearch_exporter/releases/download/v1.1.0/elasticsearch_exporter-1.1.0.linux-amd64.tar.gz"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e

apt-get -q update
install_packages ${BUILD_PACKAGES}

# Install the elastic.co repository.
(echo "deb https://artifacts.elastic.co/packages/oss-7.x/apt stable main" ;
 echo "deb [arch=amd64] https://packages.elastic.co/curator/5/debian stable main") \
    > /etc/apt/sources.list.d/elastic.list

apt-get update

# Fix openjdk-11 install failure. #955619
install -d /usr/share/man/man1

# If we do not install a JRE before ES, the installation will fail (huh?)...
install_packages default-jre-headless
install_packages ${PACKAGES}

# There is no Debian package (yet) for the prometheus
# exporter. Download the binary from github.
curl -sL ${EXPORTER_RELEASE_URL} \
    | tar --wildcards -xOzvf - '*elasticsearch_exporter' \
    > /usr/sbin/elasticsearch_exporter
chmod 755 /usr/sbin/elasticsearch_exporter

# For some reason the Debian package installs the /etc/default file
# with very restrictive permissions.
chmod 644 /etc/default/elasticsearch

# Overlay our configuration on top of /etc.
rsync -a /tmp/conf/ /etc/

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
