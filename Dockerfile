FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master

COPY elastic.gpg /etc/apt/trusted.gpg.d/
COPY conf /tmp/conf
COPY build.sh /tmp/build.sh
COPY curator-cron /usr/bin/curator-cron

RUN /tmp/build.sh && rm /tmp/build.sh

